<?php

add_action("manage_posts_custom_column",  "drubo_slider_shortcode_solumn");
add_filter("manage_edit-drubo_slider_columns", "drubo_slider_post_type_columns");
function drubo_slider_post_type_columns($columns){
  $columns = array(
    "cb" => "<input type=\"checkbox\" />",
    "title" => "Slider Name",
    "shortcode" => "Slider Shortcode",
    "Date" => "Date",
  );
  return $columns;
}
function drubo_slider_shortcode_solumn($column){
  global $post;

  switch ($column) {
    case "shortcode":
      $shortcode = "[drubo_slider slider_id=".$post->ID."]";
      echo "<h4 style='
    color: #fff;
    font-size: 18px;
    background: #000;
    overflow: hidden;
    padding: 13px;
    display: inherit;
'>$shortcode</h4>";
      break;
  }
}