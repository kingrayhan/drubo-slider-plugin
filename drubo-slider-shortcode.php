<?php

// [drubo_slider slider_id=""]
function drubo_slider_shortcode($atts,$content){
ob_start();
 
        $drubo_slider_atts = shortcode_atts(array(
                'slider_id' => '',
        ),$atts);

$slider_id = $drubo_slider_atts['slider_id'];
$args = array(
    'post_type' => array( 'drubo_slider' ),
    'p' => $slider_id
);
$query = new WP_Query( $args );
?>
<?php while ( $query->have_posts() ) : $query->the_post(); ?>
    <div class="slider-area slider-2">
        <?php  
        $sliderMeta = get_post_meta( $slider_id , 'drubo_sliders_repeatable', true ); ?>

        <div id="slider" class="nivoSlider">
        <?php   
            if(!empty($sliderMeta)):
            foreach ($sliderMeta as $key => $value) { ?>
                <img style ="display:none" src="<?php echo $value['slider_image']; ?>"  data-thumb="<?php echo $value['slider_image']; ?>"  alt="" title="#druboSliderCaption<?php echo $key; ?>"/> 
            <?php } endif; ?>  
        </div><!-- #slider -->
            <?php
            if(!empty($sliderMeta)):
            foreach ($sliderMeta as $key => $value) { ?>
                <div id="druboSliderCaption<?php echo $key; ?>" class="pos-slideshow-caption nivo-html-caption nivo-caption">
                    <div class="timing-bar"></div>
                    <div class="pos-slideshow-info pos-slideshow-info1">
                        <div class="container">
                            <div class="pos_description hidden-xs">
                                <div class="title2"><span class="txt"><?php echo $value['slider_title']; ?></span></div>
                                <div class="title1"><span class="txt"><?php echo $value['slider_subtitle']; ?></span></div>
                                <div class="pos-slideshow-readmore">
                                    <a href="<?php echo $value['slider_btn_link']; ?>" title="Explore now"><?php echo $value['slider_btn_text']; ?></a>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
            endif;


        ?>
    </div><!-- .slider-area.slider-2 -->

<?php endwhile; ?>
<?php
        wp_reset_postdata();
        return ob_get_clean();

}
add_shortcode('drubo_slider','drubo_slider_shortcode');