<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
/**
 * Plugin Name: Drubo Slider
 * Description: A simple slider plugin only for drubo theme
 * Author: Hastech
 * Author URI: http://hastech.company
 * Version: 1.0.0
*/

/**
 * Copyright (c) 2015 | rayhan095@gmail.com | All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * **********************************************************************
 */

require_once dirname(__FILE__) . '/drubo-slider-post-type.php';
require_once dirname(__FILE__) . '/drubo-slider-metabox.php';
require_once dirname(__FILE__) . '/drubo-slider-shortcode.php';
require_once dirname(__FILE__) . '/drubo-slider-shortcode-column.php';
require_once dirname(__FILE__) . '/drubo-slider-page-metabox.php';

define('DRUBO_SLIDER_PLUGIN_ACTIVATED',true);