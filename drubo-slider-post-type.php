<?php

// Register Custom Post Type
function drubo_slider_post_type_func() {

    $labels = array(
        'name'                  => _x( 'Drubo Sliders', 'Post Type General Name', 'drubo' ),
        'singular_name'         => _x( 'Drubo Slider', 'Post Type Singular Name', 'drubo' ),
        'menu_name'             => __( 'Drubo Slider', 'drubo' ),
        'name_admin_bar'        => __( 'Drubo Slider', 'drubo' ),
        'all_items'             => __( 'All slider', 'drubo' ),
        'add_new_item'          => __( 'Add New SLider', 'drubo' ),
        'add_new'               => __( 'Add New Slide', 'drubo' ),
        'new_item'              => __( 'New Slide', 'drubo' ),
        'edit_item'             => __( 'Edit Slide', 'drubo' ),
        'update_item'           => __( 'Update Slide', 'drubo' ),
        'view_item'             => __( 'View Slide', 'drubo' ),
        'view_items'            => __( 'View Silde', 'drubo' ),
        'search_items'          => __( 'Search Item', 'drubo' ),
        'not_found'             => __( 'Not found', 'drubo' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'drubo' ),
        'featured_image'        => __( 'Featured Image', 'drubo' ),
        'set_featured_image'    => __( 'Set featured image', 'drubo' ),
        'remove_featured_image' => __( 'Remove featured image', 'drubo' ),
        'use_featured_image'    => __( 'Use as featured image', 'drubo' ),
        'insert_into_item'      => __( 'Insert into item', 'drubo' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'drubo' ),
        'items_list'            => __( 'Items list', 'drubo' ),
        'items_list_navigation' => __( 'Items list navigation', 'drubo' ),
        'filter_items_list'     => __( 'Filter items list', 'drubo' ),
    );
    $args = array(
        'label'                 => __( 'Drubo Slider', 'drubo' ),
        'labels'                => $labels,
        'supports'              => array( 'title','custom-fields' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-gallery',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'drubo_slider', $args );

}
add_action( 'init', 'drubo_slider_post_type_func', 0 );