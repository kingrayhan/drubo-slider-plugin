<?php



add_action( 'cmb2_admin_init', 'drubo_slider_page_metabox' );
function drubo_slider_page_metabox() {

// Grab sliders ids
$sliders = array();
$args = array(
    'post_type' => array( 'drubo_slider' ),
);
$query = new WP_Query( $args );
while ( $query->have_posts() ) : $query->the_post();
    $sliders[get_the_id()] = get_the_title();
endwhile; 



    $prefix = 'Drubo_';
    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'drubo_slid1212er_metabox',
        'title'         => esc_html__( 'Drubo Slider', 'drubo' ),
        'object_types'  => array( 'page', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );
    ////////////////////////////////////////////////////////////////////

    $cmb->add_field(array(
        'id' => $prefix . 'enable_drubo_slider',
        'name' => esc_html__('Enable Slider','drubo'),
        'type' => 'radio',
        'options'          => array(
            '1'    => esc_html__( 'Enable', 'drubo' ),
            '2'   => esc_html__( 'Disable', 'drubo' ),
        ),
        'default' => 2,
    ));

    $cmb->add_field(array(
        'id' => $prefix . 'select_slider',
        'name' => esc_html__('Select Slider','drubo'),
        'type' => 'radio',
        'options'  => $sliders,
    ));


    ////////////////////////////////////////////////////////////////////
}