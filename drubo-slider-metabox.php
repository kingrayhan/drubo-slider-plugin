<?php
add_action( 'cmb2_admin_init', 'drubo_slider_metaboxes' );
function drubo_slider_metaboxes() {


    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'drubo_slider_metabox',
        'title'         => esc_html__( 'Drubo Slider options', 'drubo' ),
        'object_types'  => array( 'drubo_slider', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );
    ////////////////////////////////////////////////////////////////////

    $cmb->add_field(array(
    	'id' 		=> 'drubo_sliders_repeatable',
    	'name'      => __('','drubo'),
    	'type'		=> 'group',
		'options'     => array(
			'group_title'   => esc_html__( 'Drubo Slider {#}', 'drubo' ),
			'add_button'    => esc_html__( 'Add Another Entry', 'drubo' ),
			'remove_button' => esc_html__( 'Remove Entry', 'drubo' ),
			'sortable'      => true
		),

    ));

    $cmb->add_group_field('drubo_sliders_repeatable',array(
    	'id' => 'slider_image',
    	'name' => __('slider Image','drubo'),
    	'type' => 'file'
    ));


    $cmb->add_group_field('drubo_sliders_repeatable',array(
    	'id' => 'slider_title',
    	'name' => __('slider Title','drubo'),
    	'type' => 'text'
    ));

    $cmb->add_group_field('drubo_sliders_repeatable',array(
    	'id' => 'slider_subtitle',
    	'name' => __('slider Sub Title','drubo'),
    	'type' => 'text'
    ));    


    $cmb->add_group_field('drubo_sliders_repeatable',array(
    	'id' => 'slider_btn_text',
    	'name' => __('slider Button Text','drubo'),
    	'type' => 'text'
    ));

    $cmb->add_group_field('drubo_sliders_repeatable',array(
    	'id' => 'slider_btn_link',
    	'name' => __('slider Button Link','drubo'),
    	'type' => 'text_url'
    ));
    ////////////////////////////////////////////////////////////////////
}